puts "================================"
puts "======= SORTEIO DE NOMES ======="
puts "================================"

print "Digite a quantidade de pessoas que vão participar do sorteio: "
quantidade = gets.to_i
quant2 = quantidade
nomesort = Array.new

until quantidade == 0 do
   print "Pessoa numero #{quantidade}: "
   nomesort << gets
   quantidade -= 1
end

system("clear")
puts "\n - Iniciando o sorteio do amigo oculto - \n\n"

chosen = {}
chosenn = {}
raffle = {}
until quant2 == 0 do
    escolhido = nomesort.sample(1)
    sort =  nomesort.sample(1)
    cond1 = chosen.has_value? escolhido
    cond2 = chosenn.has_value? sort
    if cond1 == true and cond2 == true
      nomesort.delete(escolhido)
      nomesort.delete(sort)
      puts "Removendo nome de pessoas que já tiraram e foram tirada"
    elsif sort == escolhido
      puts "Você tirou a si mesmo, realizando o sorteio novamente"
    elsif cond1 == true
      puts "A pessoa que foi sorteada para você já saiu"
    elsif cond2 == true
      puts "Pessoa sorteada foi retirada"
    elsif sort != escolhido
      chosen[quant2] = escolhido #Guardando os resutados em uma hash só para verificar se não vai sair novamente
      chosenn[quant2] = sort # Guardando o resutado das pessoas que já sairam em um hash para não sair novamente
      raffle[sort] = escolhido # Guardando o resultado caso as condições sejam favoráveis
      print "o nome sorteado para #{sort} foi: #{escolhido} \n"
      quant2 -= 1
    end
     # nomesort.delete(chosen)
     sleep 1
     puts "\n Aperte ENTER para um novo sorteio "
     gets
     system("clear")
end

puts "Aperte ENTER para ver o resultado Final do Amigo Oculto"
gets
puts "==================================="
puts "= RESULTADO FINAL DO AMIGO OCULTO ="
puts "==================================="
raffle.each { |key, valor| puts " #{key} - Tirou: #{valor}"}

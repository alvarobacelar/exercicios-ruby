
def procura_ano_copa(ano)
   case ano
   when 1895..2005
     puts "Não Lembro "
   when 2016
     puts "Alemanha "
   when 2010
     puts "África do Sul"
   when 2014
     puts "Brasil "
   else 
     puts "Ano desconhecido "
   end
end

puts "== DESCOBRINDO QUAL PAÍS SEDIOU A COPA =="
print "Digite o ano que deseja saber: "
ano = gets.to_i
procura_ano_copa(ano)

class AmigoOculto

  # def initialize(participantes)
  #   @participantes = participantes
  # end
  def sortear
    @resultado = {}
    # participantes     = %w(DILMA LULA SARNEY TEMER CUNHA)
    sorteio = @participantes.shuffle
    sorteio.inject {|item, proximo| @resultado[item] = proximo }
    @resultado[sorteio.last] = sorteio.first

  end

  def inscrever(nome)
    # if $resultado.any?
    #   raise 'Chegou tarde demais!'
    # end
    @participantes = nome
  end

  def revelar(nome)
    @resultado[nome]
  end

  def mostrarTodos
    @resultado
  end
end

amigo = AmigoOculto.new
amigo.inscrever ["Alvaro", "Sousa", "Paulo", "Adriel"]
amigo.sortear
puts amigo.revelar "Alvaro"
# puts amigo.mostrarTodos
